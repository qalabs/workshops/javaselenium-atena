package pl.qalabs.workshops.javaselenium2._5;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NavigatingContactsPageWithActionsTests {

    WebDriver driver;

    @BeforeEach
    void setUp() {
        WebDriverManager.chromedriver().setup();
        var chromeOptions = new ChromeOptions();
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
        login();
    }

    @AfterEach
    void quitWebDriver() {
        driver.quit();
    }

    @Test
    @DisplayName("Searches and filters using keyboard only")
    void filterWithTextAndLabels() {
        // search filter field
        var searchFilter = driver.findElement(By.id("search-filter"));

        // search: click search filter, type 'ma', tab, arrow down, co-workers, arrow down, enter
        new Actions(driver).click(searchFilter)
                .sendKeys("ma")
                .sendKeys(Keys.TAB)
                .sendKeys(Keys.ARROW_DOWN)
                .sendKeys(Keys.chord("co-workers"))
                .sendKeys(Keys.ARROW_DOWN)
                .sendKeys(Keys.ENTER)
                .build().perform();

        // assert single entry in the list
        assertEquals("Emeryk Kaczmarek", driver.findElement(By.cssSelector("td > span[data-property=contact-name]")).getText());

    }

    @Test
    void toggleFavorites() {
        // more menu
        var moreMenu = driver.findElement(By.cssSelector("#contacts nav div.v-menu button"));

        // show favorites: click more menu, arrow down, enter
        new Actions(driver)
                .click(moreMenu)
                .sendKeys(Keys.ARROW_DOWN)
                .sendKeys(Keys.ENTER)
                .perform();

        // assert favorites records shown
        var contacts = driver.findElements(By.cssSelector("#contacts-list table tbody tr"));
        assertEquals(1, contacts.size());
        assertEquals("Fryderyk Maciejewski", contacts.get(0).findElement(By.cssSelector("td span[data-property=contact-name]")).getText());

        // show all: click more menu, arrow down, enter
        new Actions(driver)
                .click(moreMenu)
                .sendKeys(Keys.ARROW_DOWN)
                .sendKeys(Keys.ENTER)
                .perform();


        // assert all records shown
        contacts = driver.findElements(By.cssSelector("#contacts-list table tbody tr"));
        assertEquals(3, contacts.size());

    }

    private void login() {

        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("contacts");

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("demo");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait until contacts are visible
        var wait = new WebDriverWait(driver, 1);
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts"))));

        // wait until the logout button is visible
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#app-nav > div.v-toolbar__content > button"))));
    }
}
