package pl.qalabs.workshops.javaselenium2._2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("Contact App basic tests with class lifecycle")
class ContactAppPagesTest2 {

    WebDriver driver;

    @BeforeAll
    void setUpWebDriver() {
        WebDriverManager.chromedriver().setup();
        var chromeOptions = new ChromeOptions();
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
    }

    @BeforeEach
    void setUp() {
        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
    }

    @AfterEach
    void tearDown() {
        driver.manage().deleteAllCookies();
    }

    @AfterAll
    void quitWebDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    void isAtHomePage() {
        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Contacts App", title.getText());

        var header = driver.findElement(By.cssSelector("#home h1"));
        assertEquals("Manage your contacts with ease!", header.getText());

        var subHeader = driver.findElement(By.cssSelector("#home p"));
        assertEquals("Contacts App is a demo SPA application created in VueJS.", subHeader.getText());

        var buttons = driver.findElements(By.cssSelector("#home div.v-card__text button"));
        assertEquals(2, buttons.size());
        buttons.forEach(b -> assertTrue(b.isEnabled()));
    }

    @Test
    void navigatesToLoginPageAndBack() {
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        assertTrue(driver.getCurrentUrl().contains("/login"));

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Log in", title.getText());

        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();

        assertTrue(driver.getCurrentUrl().contains("/home"));
    }

    @Test
    void navigatesToSignupPageAndBack() {
        var signupLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Signup')]"));
        signupLink.click();

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Signup", title.getText());

        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();

        assertTrue(driver.getCurrentUrl().contains("/home"));
    }


    @Test
    void navigatesToContactUsPageAndBack() {
        var contactUsLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Contact')]"));
        contactUsLink.click();

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Contact Us", title.getText());

        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();

        assertTrue(driver.getCurrentUrl().contains("/home"));
    }

    @Test
    void opensAndClosesAboutDialog() throws InterruptedException {
        var aboutButton = driver.findElement(By.cssSelector("#app-nav > div.v-toolbar__content button"));
        aboutButton.click();

        var dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(1, dialogs.size());
        assertTrue(dialogs.get(0).isDisplayed());

        var title = dialogs.get(0).findElement(By.cssSelector("div.v-card__title.headline"));
        Thread.sleep(1000);
        assertEquals("About", title.getText());

        var closeButton = driver.findElement(By.cssSelector("#app > div.v-dialog__content--active div.v-card__actions > button > div"));
        closeButton.click();
    }

    @Test
    void opensAndClosesLoginDialog() throws InterruptedException {
        var loginButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]"));
        loginButton.click();

        var dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(1, dialogs.size());
        assertTrue(dialogs.get(0).isDisplayed());

        var title = dialogs.get(0).findElement(By.cssSelector("div.v-card__title.headline"));
        Thread.sleep(1000);
        assertEquals("Log in", title.getText());

        var cancel = driver.findElement(By.cssSelector("#login-form-actions > button.v-btn.primary--text"));
        cancel.click();

        dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(0, dialogs.size());
    }

    @Test
    void opensAndClosesSignupDialog() throws InterruptedException {
        var signupButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Signup')]"));
        signupButton.click();

        var dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(1, dialogs.size());
        assertTrue(dialogs.get(0).isDisplayed());

        var title = dialogs.get(0).findElement(By.cssSelector("div.v-card__title.headline"));
        Thread.sleep(1000);
        assertEquals("Signup", title.getText());

        var cancel = driver.findElement(By.cssSelector("#signup-form-actions > button.v-btn.primary--text"));
        cancel.click();

        dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(0, dialogs.size());
    }
}
