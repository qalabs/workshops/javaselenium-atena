package pl.qalabs.workshops.javaselenium2.pop.tests;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactsPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.HomePage;

@ExtendWith(SeleniumExtension.class)
public abstract class ContactAppTest {

    protected WebDriver driver;

    @BeforeEach
    void beforeEach(ChromeDriver driver) {
        this.driver = driver;
        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
    }

    ContactsPage loginDefault() {
        HomePage homePage = new HomePage(driver);
        return homePage.navigateToLoginPage().login("contacts", "demo");
    }
}
