package pl.qalabs.workshops.javaselenium2.pop.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactFormDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactsPage;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ParameterizedCreateContactTests extends ContactAppTest {

    private ContactFormDialog contactFormDialog;


    @BeforeEach
    void setUp() {
        var contactsPage = this.loginDefault();
        contactFormDialog = contactsPage.openContactFormDialog();
        assertTrue(contactFormDialog.isAt());
    }

    @ParameterizedTest
    @MethodSource("parametersForCreatesContactWithNameAndEmail")
    void createsContactWithNameAndEmail(String name, String email) {
        contactFormDialog.setName(name);
        contactFormDialog.setEmail(email);

        ContactsPage contactsPage = contactFormDialog.submit();

        assertTrue(contactFormDialog.isInvisible());
        assertTrue(contactsPage.getSnackbarMessage().contains("Contact created successfully."));

        List<String> names = contactsPage.getDisplayedNames();
        List<String> emails = contactsPage.getDisplayedEmails();

        assertTrue(names.contains(name));
        assertTrue(emails.contains(email));
    }

    private static Stream<Arguments> parametersForCreatesContactWithNameAndEmail() {
        return Stream.of(
                Arguments.of("A.B.", "ab@email.com"),
                Arguments.of("C.D.", "cd@email.com"),
                Arguments.of("E.F.", "ef@email.com"),
                Arguments.of("G.H.", "gh@email.com")
        );
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/contacts.csv"}, delimiter = ';')
    void createsContactWithAllProperties(String name, String email, String phone, String notes, String labels) {
        contactFormDialog.setName(name);
        contactFormDialog.setEmail(email);
        contactFormDialog.setPhone(phone);
        contactFormDialog.setNotes(notes);
        contactFormDialog.setLabels(labels.split(","));

        ContactsPage contactsPage = contactFormDialog.submit();

        assertTrue(contactFormDialog.isInvisible());
        assertTrue(contactsPage.getSnackbarMessage().contains("Contact created successfully."));

        List<String> names = contactsPage.getDisplayedNames();
        List<String> emails = contactsPage.getDisplayedEmails();

        assertTrue(names.contains(name));
        assertTrue(emails.contains(email));
    }
}
