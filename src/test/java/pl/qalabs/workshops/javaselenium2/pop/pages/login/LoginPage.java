package pl.qalabs.workshops.javaselenium2.pop.pages.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pl.qalabs.workshops.javaselenium2.pop.pages.HomePage;

public class LoginPage extends AbstractLoginPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAt() {
        return driver.getCurrentUrl().endsWith("/login");
    }

    public String getPageTitle() {
        return driver.findElement(By.cssSelector("nav div.v-toolbar__title")).getText();
    }

    public HomePage cancel() {
        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();
        return new HomePage(driver);
    }
}
