package pl.qalabs.workshops.javaselenium2.pop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContactUsPage extends AbstractPage {

    ContactUsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAt() {
        return driver.getCurrentUrl().endsWith("/contact");
    }

    public HomePage cancel() {
        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();
        return new HomePage(driver);
    }
}
