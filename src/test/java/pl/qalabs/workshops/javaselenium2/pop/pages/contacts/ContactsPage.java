package pl.qalabs.workshops.javaselenium2.pop.pages.contacts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pl.qalabs.workshops.javaselenium2.pop.pages.AbstractPage;

import java.util.List;
import java.util.stream.Collectors;

public class ContactsPage extends AbstractPage {

    private final WebElement contacts;

    public ContactsPage(WebDriver driver) {
        super(driver);
        this.contacts = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts")));
    }

    @Override
    public boolean isAt() {
        return contacts.isDisplayed();
    }

    public ContactFormDialog openContactFormDialog() {
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();
        return new ContactFormDialog(driver);
    }

    public List<String> getDisplayedNames() {
        return driver.findElements(By.cssSelector("td span[data-property=contact-name]")).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> getDisplayedEmails() {
        return driver.findElements(By.cssSelector("td span[data-property=contact-email]")).stream().map(WebElement::getText).collect(Collectors.toList());
    }
}
