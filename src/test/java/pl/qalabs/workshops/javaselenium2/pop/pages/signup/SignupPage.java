package pl.qalabs.workshops.javaselenium2.pop.pages.signup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pl.qalabs.workshops.javaselenium2.pop.pages.AbstractPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.HomePage;

public class SignupPage extends AbstractPage {

    public SignupPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAt() {
        return driver.getCurrentUrl().endsWith("/register");
    }

    public HomePage cancel() {
        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();
        return new HomePage(driver);
    }
}
