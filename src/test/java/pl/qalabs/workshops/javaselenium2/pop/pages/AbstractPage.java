package pl.qalabs.workshops.javaselenium2.pop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {

    protected final WebDriver driver;
    protected final WebDriverWait wait;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 2);
    }

    public abstract boolean isAt();

    public String getPageTitle() {
        return driver.findElement(By.cssSelector("nav div.v-toolbar__title")).getText();
    }

    public HomePage logout() {
        driver.findElement(By.cssSelector("#app-nav > div.v-toolbar__content > button")).click();
        return new HomePage(driver);
    }

    public AboutDialog openAboutDialog() {
            var aboutButton = driver.findElement(By.cssSelector("#app-nav > div.v-toolbar__content button"));
            aboutButton.click();
            return new AboutDialog(driver);
    }

    public String getSnackbarMessage() {
        // wait for the text to be present in a snackbar
        By snackbar = By.id("snackbar");
        wait.until(driver -> !driver.findElement(snackbar).getText().isEmpty());
        return driver.findElement(snackbar).getText();
    }
}
