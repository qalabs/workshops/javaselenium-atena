package pl.qalabs.workshops.javaselenium2.pop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pl.qalabs.workshops.javaselenium2.pop.pages.login.LoginDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.login.LoginPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.signup.SignupDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.signup.SignupPage;

public class HomePage extends AbstractPage {

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isAt() {
        return driver.getCurrentUrl().endsWith("/home");
    }

    public String getHeading() {
        return driver.findElement(By.cssSelector("#home h1")).getText();
    }

    public String getSubHeading() {
        return driver.findElement(By.cssSelector("#home p")).getText();
    }

    public LoginPage navigateToLoginPage() {
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();
        return new LoginPage(driver);
    }

    public SignupPage navigateToSignupPage() {
        var signupLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Signup')]"));
        signupLink.click();
        return new SignupPage(driver);
    }

    public ContactUsPage navigateToContactUsPage() {
        var contactUsLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Contact')]"));
        contactUsLink.click();
        return new ContactUsPage(driver);
    }

    public LoginDialog openLoginDialog() {
        var loginButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]"));
        loginButton.click();
        return new LoginDialog(driver);
    }

    public SignupDialog openSignupDialog() {
        var signupButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Signup')]"));
        signupButton.click();
        return new SignupDialog(driver);
    }
}
