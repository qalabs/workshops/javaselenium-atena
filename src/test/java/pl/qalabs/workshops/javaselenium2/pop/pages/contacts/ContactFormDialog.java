package pl.qalabs.workshops.javaselenium2.pop.pages.contacts;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pl.qalabs.workshops.javaselenium2.pop.pages.AbstractPage;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ContactFormDialog extends AbstractPage {

    private final WebElement form;
    private final WebElement dialog;

    ContactFormDialog(WebDriver driver) {
        super(driver);
        this.dialog = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"))
        );
        this.form = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));
    }

    public boolean isAt() {
        assertForm();
        return true;
    }

    public boolean isInvisible() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active")));
        return true;
    }

    public void setEmail(String value) {
        var email = driver.findElement(By.cssSelector("input[name=contact-email]"));
        email.clear();
        email.sendKeys(value);
    }

    public void setName(String value) {
        var name = driver.findElement(By.cssSelector("input[name=contact-name]"));
        name.clear();
        name.sendKeys(value);
    }

    public void setPhone(String value) {
        var phone = driver.findElement(By.cssSelector("input[name=contact-phone]"));
        phone.clear();
        phone.sendKeys(value);
    }

    public void setNotes(String value) {
        var notes = driver.findElement(By.cssSelector("input[name=contact-notes]"));
        notes.clear();
        notes.sendKeys(value);
    }

    public void setLabels(String... value) {
        var labels = driver.findElement(By.cssSelector("#contact-form div.v-select__slot > div.v-select__selections"));

        Actions actions = new Actions(driver);
        actions.click(labels);
        Stream.of(value).forEach(v -> {
            actions.sendKeys(v);
            actions.sendKeys(Keys.ENTER);
        });
        actions.build().perform();
    }

    public void submitExpectingErrors() {
        var submitButton = driver.findElement(By.cssSelector("button[type=submit]"));
        submitButton.click();
    }


    public ContactsPage submit() {
        var submitButton = driver.findElement(By.cssSelector("button[type=submit]"));
        submitButton.click();
        return new ContactsPage(driver);
    }

    public List<String> getErrorMessages() {
        // wait until error messages are visible
        List<WebElement> errors = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#contact-form .v-text-field__details .error--text .v-messages__message")));
        List<String> errorMessages = errors.stream().map(WebElement::getText).collect(Collectors.toList());
        return errorMessages;
    }


    private void assertForm() {
        // wait avatar has default image set
        wait.until(driver -> form.findElement(By.cssSelector(".v-avatar .v-image__image.v-image__image--cover"))
                .getCssValue("background-image")
                .contains("https://ssl.gstatic.com/s2/oz/images/sge/grey_silhouette.png"));

        // assert name has required attribute and it is of type text and it's empty
        var name = form.findElement(By.cssSelector("input[name=contact-name]"));
        assertTrue(Boolean.valueOf(name.getAttribute("required")));
        assertTrue(name.getAttribute("value").isEmpty());

        // assert email is of type email and it's empty
        var email = form.findElement(By.cssSelector("input[name=contact-email]"));
        assertEquals("email", email.getAttribute("type"));
        assertTrue(email.getAttribute("value").isEmpty());

        // assert phone is of type tel and it's empty
        var phone = form.findElement(By.cssSelector("input[name=contact-phone]"));
        assertEquals("tel", phone.getAttribute("type"));
        assertTrue(phone.getAttribute("value").isEmpty());

        // assert notes is of type text and it's empty
        var notes = form.findElement(By.cssSelector("input[name=contact-notes]"));
        assertEquals("text", notes.getAttribute("type"));
        assertTrue(notes.getAttribute("value").isEmpty());

        // assert no labels selected (chips)
        var chips = form.findElements(By.cssSelector(".v-chip"));
        assertEquals(0, chips.size());
    }
}
