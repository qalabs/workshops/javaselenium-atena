package pl.qalabs.workshops.javaselenium2.pop.pages.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pl.qalabs.workshops.javaselenium2.pop.pages.AbstractPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactsPage;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractLoginPage extends AbstractPage {

    AbstractLoginPage(WebDriver driver) {
       super(driver);
    }

    public String loginExpectingSnackbarMessage(String username, String password) {
        doLogin(username, password);

        // wait for the text to be present in a snackbar
        By snackbar = By.id("snackbar");
        wait.until(driver -> !driver.findElement(snackbar).getText().isEmpty());
        return driver.findElement(snackbar).getText();
    }

    public List<String> loginExpectingErrorMessages(String username, String password) {
        doLogin(username, password);

        // wait for the text to be present in a snackbar
        List<WebElement> errors = wait.until(driver -> driver.findElements(By.cssSelector("#login-form .v-text-field__details .error--text .v-messages__message")));
        List<String> errorMessages = errors.stream().map(WebElement::getText).collect(Collectors.toList());
        return errorMessages;
    }

    public ContactsPage login(String username, String password) {
        doLogin(username, password);
        return new ContactsPage(driver);
    }

    private void doLogin(String username, String password) {
        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys(username);

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys(password);

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();
    }
}
