package pl.qalabs.workshops.javaselenium2.pop.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.qalabs.workshops.javaselenium2.pop.pages.AboutDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.ContactUsPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.HomePage;
import pl.qalabs.workshops.javaselenium2.pop.pages.login.LoginDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.login.LoginPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.signup.SignupDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.signup.SignupPage;

import static org.junit.jupiter.api.Assertions.*;

class BetterHomePageTests extends ContactAppTest {

    HomePage homePage;

    @BeforeEach
    void setUp() {
        homePage = new HomePage(driver);
    }

    @Test
    void isAtHomePage() {
        assertAll(
                () -> assertTrue(homePage.isAt()),
                () -> assertEquals("Contacts App", homePage.getPageTitle()),
                () -> assertEquals("Manage your contacts with ease!", homePage.getHeading()),
                () -> assertEquals("Contacts App is a demo SPA application created in VueJS.", homePage.getSubHeading())
        );
    }

    @Test
    void navigatesToLoginPageAndBack() {
        LoginPage loginPage = homePage.navigateToLoginPage();
        assertTrue(loginPage.isAt());
        assertEquals("Log in", loginPage.getPageTitle());
        homePage = loginPage.cancel();
        assertTrue(homePage.isAt());
    }

    @Test
    void navigatesToSignupPageAndBack() {
        SignupPage signupPage = homePage.navigateToSignupPage();
        assertTrue(signupPage.isAt());
        assertEquals("Signup", signupPage.getPageTitle());
        homePage = signupPage.cancel();
        assertTrue(homePage.isAt());
    }


    @Test
    void navigatesToContactUsPageAndBack() {
        ContactUsPage contactUsPage = homePage.navigateToContactUsPage();
        assertTrue(contactUsPage.isAt());
        assertEquals("Contact Us", contactUsPage.getPageTitle());
        homePage = contactUsPage.cancel();
        assertTrue(homePage.isAt());
    }

    @Test
    void opensAndClosesAboutDialog() {
        AboutDialog aboutDialog = homePage.openAboutDialog();
        assertTrue(aboutDialog.isAt());
        assertEquals("About", aboutDialog.getDialogTitle());
        aboutDialog.cancel();
        homePage.isAt();
    }

    @Test
    void opensAndClosesLoginDialog() {
        LoginDialog loginDialog = homePage.openLoginDialog();
        assertTrue(loginDialog.isAt());
        assertEquals("Log in", loginDialog.getDialogTitle());
        loginDialog.cancel();
        homePage.isAt();
    }

    @Test
    void opensAndClosesSignupDialog() {
        SignupDialog signupDialog = homePage.openSignupDialog();
        assertTrue(signupDialog.isAt());
        assertEquals("Signup", signupDialog.getDialogTitle());
        signupDialog.cancel();
        homePage.isAt();
    }
}
