package pl.qalabs.workshops.javaselenium2.pop.tests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HomePageTests extends ContactAppTest {

    @Test
    void isAtHomePage() {

        assertTrue(driver.getCurrentUrl().endsWith("/home"));

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Contacts App", title.getText());

        var header = driver.findElement(By.cssSelector("#home h1"));
        assertEquals("Manage your contacts with ease!", header.getText());

        var subHeader = driver.findElement(By.cssSelector("#home p"));
        assertEquals("Contacts App is a demo SPA application created in VueJS.", subHeader.getText());
    }

    @Test
    void navigatesToLoginPageAndBack() {
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        assertTrue(driver.getCurrentUrl().contains("/login"));

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Log in", title.getText());

        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();

        assertTrue(driver.getCurrentUrl().contains("/home"));
    }

    @Test
    void navigatesToSignupPageAndBack() {
        var signupLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Signup')]"));
        signupLink.click();

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Signup", title.getText());

        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();

        assertTrue(driver.getCurrentUrl().contains("/home"));
    }


    @Test
    void navigatesToContactUsPageAndBack() {
        var contactUsLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Contact')]"));
        contactUsLink.click();

        var title = driver.findElement(By.cssSelector("nav div.v-toolbar__title"));
        assertEquals("Contact Us", title.getText());

        var cancelButton = driver.findElement(By.xpath("//button/div[contains(text(), 'Cancel')]"));
        cancelButton.click();

        assertTrue(driver.getCurrentUrl().contains("/home"));
    }

    @Test
    void opensAndClosesAboutDialog() throws InterruptedException {
        var aboutButton = driver.findElement(By.cssSelector("#app-nav > div.v-toolbar__content button"));
        aboutButton.click();

        var dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(1, dialogs.size());
        assertTrue(dialogs.get(0).isDisplayed());

        var title = dialogs.get(0).findElement(By.cssSelector("div.v-card__title.headline"));
        Thread.sleep(1000);
        assertEquals("About", title.getText());

        var closeButton = driver.findElement(By.cssSelector("#app > div.v-dialog__content--active div.v-card__actions > button > div"));
        closeButton.click();
    }

    @Test
    void opensAndClosesLoginDialog() throws InterruptedException {
        var loginButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]"));
        loginButton.click();

        var dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(1, dialogs.size());
        assertTrue(dialogs.get(0).isDisplayed());

        var title = dialogs.get(0).findElement(By.cssSelector("div.v-card__title.headline"));
        Thread.sleep(1000);
        assertEquals("Log in", title.getText());

        var cancel = driver.findElement(By.cssSelector("#login-form-actions > button.v-btn.primary--text"));
        cancel.click();

        dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(0, dialogs.size());
    }

    @Test
    void opensAndClosesSignupDialog() throws InterruptedException {
        var signupButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Signup')]"));
        signupButton.click();

        var dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(1, dialogs.size());
        assertTrue(dialogs.get(0).isDisplayed());

        var title = dialogs.get(0).findElement(By.cssSelector("div.v-card__title.headline"));
        Thread.sleep(1000);
        assertEquals("Signup", title.getText());

        var cancel = driver.findElement(By.cssSelector("#signup-form-actions > button.v-btn.primary--text"));
        cancel.click();

        dialogs = driver.findElements(By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"));
        assertEquals(0, dialogs.size());
    }
}
