package pl.qalabs.workshops.javaselenium2.pop.pages.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginDialog extends AbstractLoginPage {

    private final WebElement dialog;

    public LoginDialog(WebDriver driver) {
        super(driver);
        this.dialog = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active"))
        );
    }

    public boolean isAt() {
        return dialog.isDisplayed();
    }

    public String getDialogTitle() {
        return dialog.findElement(By.cssSelector("div.v-card__title.headline.grey.lighten-2.v-card__title--primary")).getText();
    }

    public void cancel() {
        var cancel = driver.findElement(By.cssSelector("#login-form-actions > button.v-btn.primary--text"));
        cancel.click();
    }
}
