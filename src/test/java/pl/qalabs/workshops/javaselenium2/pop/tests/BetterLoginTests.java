package pl.qalabs.workshops.javaselenium2.pop.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.qalabs.workshops.javaselenium2.pop.pages.HomePage;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactsPage;
import pl.qalabs.workshops.javaselenium2.pop.pages.login.LoginDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.login.LoginPage;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BetterLoginTests extends ContactAppTest {

    HomePage homePage;

    @BeforeEach
    void setUp() {
        homePage = new HomePage(driver);
    }

    @Test
    void loginExpectingSnackbarMessage() {
        LoginPage loginPage = homePage.navigateToLoginPage();
        String message = loginPage.loginExpectingSnackbarMessage("invalid", "invalid");
        assertTrue(message.contains("Invalid credentials"), "Invalid message: " + message);
    }

    @Test
    void loginExpectingErrorMessages() {
        LoginPage loginPage = homePage.navigateToLoginPage();
        List<String> errors = loginPage.loginExpectingErrorMessages("", "");
        assertLinesMatch(List.of("Username is required.", "Password is required."), errors);
    }

    @Test
    void loginSuccessfully() {
        LoginPage loginPage = homePage.navigateToLoginPage();
        ContactsPage contactsPage = loginPage.login("contacts", "demo");
        assertTrue(contactsPage.isAt());
        assertEquals("Contacts", contactsPage.getPageTitle());
        homePage = contactsPage.logout();
        assertTrue(homePage.isAt());
    }

    @Test
    void loginFromDialogExpectingSnackbarMessage() {
        LoginDialog loginDialog = homePage.openLoginDialog();
        String message = loginDialog.loginExpectingSnackbarMessage("invalid", "invalid");
        assertTrue(message.contains("Invalid credentials"), "Invalid message: " + message);
    }

    @Test
    void loginFromDialogExpectingErrorMessages() {
        LoginDialog loginDialog = homePage.openLoginDialog();
        List<String> errors = loginDialog.loginExpectingErrorMessages("", "");
        assertLinesMatch(List.of("Username is required.", "Password is required."), errors);
    }

    @Test
    void loginSuccessfullyFromDialog() {
        LoginDialog loginDialog = homePage.openLoginDialog();
        ContactsPage contactsPage = loginDialog.login("contacts", "demo");
        assertTrue(contactsPage.isAt());
        assertEquals("Contacts", contactsPage.getPageTitle());
        homePage = contactsPage.logout();
        assertTrue(homePage.isAt());
    }
}
