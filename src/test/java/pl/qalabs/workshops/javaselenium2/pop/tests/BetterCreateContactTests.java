package pl.qalabs.workshops.javaselenium2.pop.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactFormDialog;
import pl.qalabs.workshops.javaselenium2.pop.pages.contacts.ContactsPage;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BetterCreateContactTests extends ContactAppTest {

    private ContactFormDialog contactFormDialog;

    @BeforeEach
    void setUp() {
        var contactsPage = this.loginDefault();
        contactFormDialog = contactsPage.openContactFormDialog();
        assertTrue(contactFormDialog.isAt());
    }

    @Test
    void createsContactExpectingNameErrorMessage() {
        contactFormDialog.submitExpectingErrors();
        assertLinesMatch(List.of("Name is required."), contactFormDialog.getErrorMessages());
    }

    @Test
    void setInvalidEmailExpectingErrorMessage() {
        contactFormDialog.setEmail("invalid-email");
        assertLinesMatch(List.of("Email must be a valid email address"), contactFormDialog.getErrorMessages());
    }

    @Test
    void createsContactWithNameAndEmail() {
        contactFormDialog.setName("Jan Kowalski");
        contactFormDialog.setEmail("jan.kowalski@qalabs.pl");

        ContactsPage contactsPage = contactFormDialog.submit();

        assertTrue(contactFormDialog.isInvisible());
        assertTrue(contactsPage.getSnackbarMessage().contains("Contact created successfully."));

        List<String> names = contactsPage.getDisplayedNames();
        List<String> emails = contactsPage.getDisplayedEmails();

        assertTrue(names.contains("Jan Kowalski"));
        assertTrue(emails.contains("jan.kowalski@qalabs.pl"));
    }

    @Test
    void createsContactWithAllProperties() {
        contactFormDialog.setName("Ola Nowak");
        contactFormDialog.setEmail("ola.nowak@qalabs.pl");
        contactFormDialog.setPhone("(58) 321-12-34");
        contactFormDialog.setNotes("www.lipsum.org");
        contactFormDialog.setLabels("Friends", "Co-workers");

        ContactsPage contactsPage = contactFormDialog.submit();

        assertTrue(contactFormDialog.isInvisible());
        assertTrue(contactsPage.getSnackbarMessage().contains("Contact created successfully."));

        List<String> names = contactsPage.getDisplayedNames();
        List<String> emails = contactsPage.getDisplayedEmails();

        assertTrue(names.contains("Ola Nowak"));
        assertTrue(emails.contains("ola.nowak@qalabs.pl"));
    }
}
