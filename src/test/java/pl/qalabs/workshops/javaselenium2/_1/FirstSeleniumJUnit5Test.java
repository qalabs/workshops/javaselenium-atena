package pl.qalabs.workshops.javaselenium2._1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

class FirstSeleniumJUnit5Test {

    private static final Logger LOG = LoggerFactory.getLogger(FirstSeleniumJUnit5Test.class);

    @Test
    void projectIsConfiguredProperly() {

        LOG.info("Running my first Selenium and JUnit 5 test!");

        // setup
        WebDriverManager.chromedriver().setup();
        var driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);

        // test
        driver.get("https://qalabs.pl");
        Assertions.assertEquals("QA Labs - Warsztaty dla Specjalistów IT", driver.getTitle());

        // teardown
        driver.quit();
    }
}
