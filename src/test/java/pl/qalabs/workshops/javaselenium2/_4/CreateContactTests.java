package pl.qalabs.workshops.javaselenium2._4;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Create 'contact' tests")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateContactTests {

    WebDriver driver;

    @BeforeAll
    void setUpWebDriver() {
        WebDriverManager.chromedriver().setup();
        var chromeOptions = new ChromeOptions();
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
    }

    @BeforeEach
    void setUp() {
        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
        login();
    }

    @AfterAll
    void quitWebDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    void assertsContactFormFields() {

        // click the button and wait for the dialog to be visible
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();

        var wait = new WebDriverWait(driver, 1);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // wait avatar has default image set
        assertDoesNotThrow(() -> wait.until(webDriver -> webDriver.findElement(By.cssSelector("#contact-form .v-avatar .v-image__image.v-image__image--cover")).getCssValue("background-image").contains("https://ssl.gstatic.com/s2/oz/images/sge/grey_silhouette.png")));

        // assert name has required attribute and it is of type text and it's empty
        var name = driver.findElement(By.cssSelector("input[name=contact-name]"));
        assertTrue(Boolean.valueOf(name.getAttribute("required")));
        assertTrue(name.getAttribute("value").isEmpty());

        // assert email is of type email and it's empty
        var email = driver.findElement(By.cssSelector("input[name=contact-email]"));
        assertEquals("email", email.getAttribute("type"));
        assertTrue(email.getAttribute("value").isEmpty());

        // assert phone is of type tel and it's empty
        var phone = driver.findElement(By.cssSelector("input[name=contact-phone]"));
        assertEquals("tel", phone.getAttribute("type"));
        assertTrue(phone.getAttribute("value").isEmpty());

        // assert notes is of type text and it's empty
        var notes = driver.findElement(By.cssSelector("input[name=contact-notes]"));
        assertEquals("text", notes.getAttribute("type"));
        assertTrue(notes.getAttribute("value").isEmpty());

        // assert no labels selected (chips)
        var chips = driver.findElements(By.cssSelector("#contact-form .v-chip"));
        assertEquals(0, chips.size());
    }

    @Test
    void createsContactExpectingNameErrorMessage() {

        // click the button and wait for the dialog to be visible
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();

        var wait = new WebDriverWait(driver, 1);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // submit the form without filling it
        var submitButton = driver.findElement(By.cssSelector("button[type=submit]"));
        submitButton.click();

        // assert error message
        var errors = wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("#contact-form .v-text-field__details .error--text"), 1));
        assertEquals(1, errors.size());

        // wait until error messages are visible
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#contact-form .v-text-field__details .error--text .v-messages__message")));
        assertLinesMatch(List.of("Name is required."),
                errors.stream().map(WebElement::getText).collect(Collectors.toList()));
    }

    @Test
    void setInvalidEmailExpectingErrorMessage() {

        // click the button and wait for the dialog to be visible
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();

        var wait = new WebDriverWait(driver, 1);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // invalid email (1)
        var email = driver.findElement(By.cssSelector("input[name=contact-email]"));
        email.clear();
        email.sendKeys(Keys.chord("1", "2", "3"));

        // assert error message
        var errors = wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("#contact-form .v-text-field__details .error--text"), 1));
        assertEquals(1, errors.size());

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#contact-form .v-text-field__details .error--text .v-messages__message")));
        assertLinesMatch(List.of("Email must be a valid email address"),
                errors.stream().map(WebElement::getText).collect(Collectors.toList()));
    }

    @Test
    void createsContactWithNameAndEmail() {

        // click the button and wait for the dialog to be visible
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();

        var wait = new WebDriverWait(driver, 1);
        var form = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // name
        var name = driver.findElement(By.cssSelector("input[name=contact-name]"));
        name.clear();
        name.sendKeys("Jan Kowalski");

        // email
        var email = driver.findElement(By.cssSelector("input[name=contact-email]"));
        email.clear();
        email.sendKeys("jan.kowalski@qalabs.pl");

        // submit
        var submitButton = driver.findElement(By.cssSelector("button[type=submit]"));
        submitButton.click();

        // make sure dialog is gone
        wait.until(ExpectedConditions.invisibilityOf(form));

        // assert snackbar message
        assertDoesNotThrow(() -> ExpectedConditions.textToBePresentInElementLocated(By.id("snacbkar"), "Contact created successfully."));

        // assert new contact is in the list
        List<String> names = driver.findElements(By.cssSelector("td span[data-property=contact-name]")).stream().map(WebElement::getText).collect(Collectors.toList());
        List<String> emails = driver.findElements(By.cssSelector("td span[data-property=contact-email]")).stream().map(WebElement::getText).collect(Collectors.toList());

        assertTrue(names.contains("Jan Kowalski"));
        assertTrue(emails.contains("jan.kowalski@qalabs.pl"));
    }

    @Test
    void createsContactWithAllProperties() {

        // click the button and wait for the dialog to be visible
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();

        var wait = new WebDriverWait(driver, 1);
        var form = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // name
        var name = driver.findElement(By.cssSelector("input[name=contact-name]"));
        name.clear();
        name.sendKeys("Ola Nowak");

        // email
        var email = driver.findElement(By.cssSelector("input[name=contact-email]"));
        email.clear();
        email.sendKeys("ola.nowak@qalabs.pl");

        // phone
        var phone = driver.findElement(By.cssSelector("input[name=contact-phone]"));
        phone.clear();
        phone.sendKeys("(58) 321-12-34");

        // notes
        var notes = driver.findElement(By.cssSelector("input[name=contact-notes]"));
        notes.clear();
        notes.sendKeys("www.lipsum.org");

        // labels (with actions)
        var labels = driver.findElement(By.cssSelector("#contact-form div.v-select__slot > div.v-select__selections"));

        Actions actions = new Actions(driver);
        actions.click(labels);
        actions.sendKeys(Keys.ARROW_DOWN);
        actions.sendKeys(Keys.ENTER);
        actions.sendKeys(Keys.ARROW_DOWN);
        actions.sendKeys(Keys.ENTER);
        actions.build().perform();

        // submit
        var submitButton = driver.findElement(By.cssSelector("button[type=submit]"));
        submitButton.click();

        // make sure dialog is gone
        wait.until(ExpectedConditions.invisibilityOf(form));

        // assert snackbar message
        assertDoesNotThrow(() -> ExpectedConditions.textToBePresentInElementLocated(By.id("snacbkar"), "Contact created successfully."));

        // assert new contact is in the list
        List<String> names = driver.findElements(By.cssSelector("td span[data-property=contact-name]")).stream().map(WebElement::getText).collect(Collectors.toList());
        List<String> emails = driver.findElements(By.cssSelector("td span[data-property=contact-email]")).stream().map(WebElement::getText).collect(Collectors.toList());

        assertTrue(names.contains("Ola Nowak"));
        assertTrue(emails.contains("ola.nowak@qalabs.pl"));
    }

    @Test
    void createsContactWithAvatar() {

        // click the button and wait for the dialog to be visible
        var newContactButton = driver.findElement(By.xpath("//button//*[contains(text(), 'New Contact')]"));
        newContactButton.click();

        var wait = new WebDriverWait(driver, 1);
        var form = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // name
        var name = driver.findElement(By.cssSelector("input[name=contact-name]"));
        name.clear();
        name.sendKeys("Tomasz Nowak");

        // email
        var email = driver.findElement(By.cssSelector("input[name=contact-email]"));
        email.clear();
        email.sendKeys("tomasz.nowak@qalabs.pl");

        // avatar upload
        var fileInput = driver.findElement(By.cssSelector("input[type=file]"));
        var file = new File(getClass().getResource("/cat.png").getFile());

        fileInput.sendKeys(file.getAbsolutePath());

        // verify avatar image is different than default
        assertDoesNotThrow(() -> wait.until(webDriver -> webDriver.findElement(By.cssSelector("#contact-form .v-avatar .v-image__image.v-image__image--cover")).getCssValue("background-image").contains("data:image/png")));

        // submit
        var submitButton = driver.findElement(By.cssSelector("button[type=submit]"));
        submitButton.click();

        // make sure dialog is gone
        wait.until(ExpectedConditions.invisibilityOf(form));

        // assert snackbar message
        assertDoesNotThrow(() -> ExpectedConditions.textToBePresentInElementLocated(By.id("snacbkar"), "Contact created successfully."));

        // assert new contact is in the list
        List<String> names = driver.findElements(By.cssSelector("td span[data-property=contact-name]")).stream().map(WebElement::getText).collect(Collectors.toList());
        List<String> emails = driver.findElements(By.cssSelector("td span[data-property=contact-email]")).stream().map(WebElement::getText).collect(Collectors.toList());

        assertTrue(names.contains("Tomasz Nowak"));
        assertTrue(emails.contains("tomasz.nowak@qalabs.pl"));
    }

    private void login() {

        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("contacts");

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("demo");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait until contacts are visible
        var wait = new WebDriverWait(driver, 1);
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts"))));

        // wait until the logout button is visible
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#app-nav > div.v-toolbar__content > button"))));
    }
}
