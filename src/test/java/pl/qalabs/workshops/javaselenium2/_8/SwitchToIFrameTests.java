package pl.qalabs.workshops.javaselenium2._8;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Switch to iframe in a test")
@ExtendWith(SeleniumExtension.class)
class SwitchToIFrameTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchToIFrameTests.class);

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp(ChromeDriver driver) {
        this.driver = driver;
        this.driver.manage().window().maximize();
        this.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 1);
    }

    @Test
    void switchesToIFrame() {
        driver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_iframe_border2");

        // switch to iframe using wait object
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("iframeResult")));

        // get h2 of the iframe
        assertEquals("Custom Iframe Border", driver.findElement(By.tagName("h2")).getText());

        // switch to iframe using wait object
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.cssSelector("body > iframe")));

        // get h1 of the iframe
        assertEquals("This page is displayed in an iframe", driver.findElement(By.tagName("h1")).getText());

        // switch back to default content
        driver.switchTo().defaultContent();
    }
}
