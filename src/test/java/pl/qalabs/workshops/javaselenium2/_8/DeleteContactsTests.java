package pl.qalabs.workshops.javaselenium2._8;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DisplayName("Deleting contacts with confirmation")
@ExtendWith(SeleniumExtension.class)
class DeleteContactsTests {

    private WebDriver driver;

    @BeforeEach
    void initialize(ChromeDriver driver) {
        this.driver = driver;
        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
        login();
    }

    @Test
    void deleteAllContacts() {
        // select all records
        var contacts = driver.findElement(By.id("contacts-list"));
        var selectAllCheckbox = contacts.findElement(By.cssSelector("table > thead .v-input--selection-controls__input"));
        selectAllCheckbox.click();

        // delete
        var deleteButton = driver.findElement(By.xpath("(//*[@id='contacts']//i[text()='delete']/..)[1]"));
        deleteButton.click();

        // dismiss the alert
        driver.switchTo().alert().dismiss();

        // click delete button again
        deleteButton.click();

        // accept the alert
        driver.switchTo().alert().accept();

        // assert no data element exists
        assertDoesNotThrow(() -> driver.findElement(By.cssSelector("#no-data")));

    }

    private void login() {

        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("contacts");

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("demo");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait until contacts are visible
        var wait = new WebDriverWait(driver, 1);
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts"))));

        // wait until the logout button is visible
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#app-nav > div.v-toolbar__content > button"))));
    }
}
