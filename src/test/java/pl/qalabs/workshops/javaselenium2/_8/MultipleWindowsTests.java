package pl.qalabs.workshops.javaselenium2._8;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

@DisplayName("Managing windows handles in tests")
@ExtendWith(SeleniumExtension.class)
class MultipleWindowsTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleWindowsTests.class);

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp(ChromeDriver driver) {
        this.driver = driver;
        this.driver.manage().window().maximize();
        this.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 1);
    }

    @Test
    void theTest() throws InterruptedException {

        driver.get("https://www.meteo.pl");

        // select the model
        driver.findElement(By.id("tab_um")).click();

        // wait form model to be loaded
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("#info_coamps > table > tbody > tr > td:nth-child(1) > font"), "MODEL UM"));

        // get current window handle
        String rootWindow = driver.getWindowHandle();

        // select other city
        driver.findElement(By.cssSelector("#kon_3c_b > div:nth-child(2) > div > table > tbody > tr:nth-child(3) > td > div:nth-child(2) > a")).click();

        // print all windows handles
        driver.getWindowHandles().forEach(wh -> LOGGER.info("Window handle: " + wh));

        // get current window handle, excluding the root window
        String secondWindow = driver.getWindowHandles().stream().filter(h -> !h.equals(rootWindow)).findFirst().get();

        // switch to the second window
        driver = driver.switchTo().window(secondWindow);
        Thread.sleep(1000); // Don't know why

        // wait for page to be loaded
        ExpectedCondition<Boolean> pageLoadCondition = driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
        wait.until(pageLoadCondition);

        // input text
        WebElement element =  driver.findElement(By.cssSelector("input[type=text]"));
        element.sendKeys("zukowo");
        element.sendKeys(Keys.ENTER);

        // click the button
        driver.findElement(By.cssSelector("body > table > tbody > tr:nth-child(2) > td:nth-child(2) > a")).click();

        // get third window handle excluding root and second windows
        String thirdWindow = driver.getWindowHandles()
                .stream()
                .filter(h -> !h.equals(rootWindow) && !h.equals(secondWindow)).findFirst().get();

        // switch to third window
        driver = driver.switchTo().window(thirdWindow);
        // wait for page to be loaded
        wait.until(pageLoadCondition);
        // closes the current window
        driver.close();
        // switch back to the second window and close it
        driver.switchTo().window(secondWindow).close();
        // switch back to the first window (don't need to close it!)
        driver.switchTo().window(rootWindow);
    }

}
