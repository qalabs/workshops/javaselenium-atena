package pl.qalabs.workshops.javaselenium2._7;

import io.github.bonigarcia.seljup.Options;
import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SeleniumExtension.class)
class DownloadContactsInFirefoxTests {

    private WebDriver driver;

    // create temporary directory to be used for downloading files
    @TempDir
    static Path downloadDir;

    // create Firefox options to be injected into the driver
    @Options
    private FirefoxOptions firefoxOptions = new FirefoxOptions();

    {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", downloadDir.toString());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");

        firefoxOptions.setProfile(profile);
    }

    @BeforeEach
    void initialize(FirefoxDriver driver) throws URISyntaxException, IOException {

        this.driver = driver;

        // initialize records from JSON
        Path path = Paths.get(getClass().getResource("/contacts.json").toURI());
        String json = Files.readString(path);

        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
        driver.executeScript(
                "window.localStorage.clear(); window.localStorage.setItem('contacts-app-data', arguments[0])", json
        );
        driver.manage().window().maximize();

        // or
        // driver.getLocalStorage().clear();
        // driver.getLocalStorage().setItem("contacts-app-data", json);

        login();
    }

    @Test
    void downloadAllContactsInFirefox() throws IOException {
        // select all records
        var contacts = driver.findElement(By.id("contacts-list"));
        var selectAllCheckbox = contacts.findElement(By.cssSelector("table > thead .v-input--selection-controls__input"));
        selectAllCheckbox.click();

        // download
        var downloadButton = driver.findElement(By.xpath("//*[@id='contacts']//i[text()='get_app']/.."));
        downloadButton.click();

        // wait for file to be downloaded by filtering out the temporary dir until contacts.csv file is there
        WebDriverWait wait = new WebDriverWait(driver, 2);
        Path contactsFile = wait.until(d -> {
            try {
                return Files.list(downloadDir).filter(path -> path.endsWith("contacts.csv")).findFirst().orElse(null);
            } catch (IOException e) {
                return null;
            }
        });;

        // assert file content
        assertNotNull(contactsFile);

        String contactsAsString = Files.readString(contactsFile);
        String expectedContactsAsString = "Name,Email,Phone,Labels,Favorite\n" +
                "\"John Maciejewski\",john.maciejewski@teleworm.us,195 24 07 01,\"Co-workers\",false\n" +
                "\"Jane Maciejewski\",jane.maciejewski@teleworm.us,195 24 07 02,\"Co-workers\",false\n" +
                "\"Doug Lee\",doug.kee@teleworm.us,195 24 07 03,\"Co-workers\",false\n" +
                "\"Lee Woo\",lee.woo@teleworm.us,195 24 07 04,\"Co-workers\",false\n" +
                "\"Law Done\",law.done@teleworm.us,195 24 07 05,\"Co-workers\",false";

        assertEquals(expectedContactsAsString, contactsAsString);
    }

    private void login() {

        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("contacts");

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("demo");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait until contacts are visible
        var wait = new WebDriverWait(driver, 1);
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts"))));

        // wait until the logout button is visible
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#app-nav > div.v-toolbar__content > button"))));
    }
}
