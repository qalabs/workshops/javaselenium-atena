package pl.qalabs.workshops.javaselenium2._3;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Login tests with fluent waits")
class LoginTests {

    WebDriver driver;

    @BeforeAll
    static void webDriverManager() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setUpWebDriver() {
        var chromeOptions = new ChromeOptions();
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        driver.get("https://qalabs.gitlab.io/vuejs-contacts-demo/");
    }

    @AfterEach
    void quitWebDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    void loginExpectingSnackbarMessage() {
        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("invalid");

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("invalid");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait for the text to be present in a snackbar and make sure no timeout exception is thrown
        var wait = new WebDriverWait(driver, 1);
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Invalid credentials")));
    }

    @Test
    void loginExpectingErrorMessages() {
        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait until the number of errors is 2
        var wait = new WebDriverWait(driver, 1);
        var errors = wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("#login-form .v-text-field__details .error--text"), 2));
        assertEquals(2, errors.size());

        // wait until error messages are visible
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#login-form .v-text-field__details .error--text .v-messages__message")));
        assertLinesMatch(List.of("Username is required.", "Password is required."),
                errors.stream().map(WebElement::getText).collect(Collectors.toList()));
    }

    @Test
    void loginSuccessfully() {
        // click the login link
        var loginLink = driver.findElement(By.xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]"));
        loginLink.click();

        // fill in the username
        var usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("contacts");

        // fill in the password
        var passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("demo");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // wait until contacts are visible
        var wait = new WebDriverWait(driver, 1);
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts"))));

        // wait until the logout button is visible
        var logoutButton = assertDoesNotThrow(() -> wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#app-nav > div.v-toolbar__content > button"))));
        // logout
        logoutButton.click();

        // wait until the current url contains '/#/home'
        assertDoesNotThrow(() -> wait.until(webDriver -> webDriver.getCurrentUrl().contains("/#/home")));
    }

    @Test
    void loginFromDialogExpectingSnackbarMessage() {
        // click the login button
        var loginButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]"));
        loginButton.click();

        // wait until username field is visible
        var wait = new WebDriverWait(driver, 1);
        var usernameField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        // fill in the username field
        usernameField.sendKeys("invalid");

        // wait until password field is visible
        var passwordField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        // fill in the password field
        passwordField.sendKeys("invalid");

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("#login-form-actions > button[type=submit]"));
        submitButton.click();

        // wait for the text to be present in a snackbar and make sure no timeout exception is thrown
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Invalid credentials.")));
    }

    @Test
    void loginFromDialogExpectingErrorMessages() {
        // click the login button
        var loginButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]"));
        loginButton.click();

        // wait until the username field is visible
        var wait = new WebDriverWait(driver, 1);
        var usernameField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));

        // clear the username field
        usernameField.clear();

        // wait until the password field is visible
        var passwordField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));

        // clear the password field
        passwordField.clear();

        // submit the form
        var submitButton = driver.findElement(By.cssSelector("#login-form-actions > button[type=submit]"));
        submitButton.click();

        // wait until the number of errors is 2
        var errors = wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("#login-form .v-text-field__details .error--text"), 2));
        assertEquals(2, errors.size());

        // wait until error messages are visible
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#login-form .v-text-field__details .error--text .v-messages__message")));
        assertLinesMatch(List.of("Username is required.", "Password is required."),
                errors.stream().map(WebElement::getText).collect(Collectors.toList()));
    }

    @Test
    void loginSuccessfullyFromDialog() {
        // click the login button
        var loginButton = driver.findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]"));
        loginButton.click();

        // wait until the username field is visible
        var wait = new WebDriverWait(driver, 1);
        var usernameField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));

        // fill in the username field
        usernameField.sendKeys("contacts");

        // wait until the password field is visible
        var password = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        password.sendKeys("demo");

        // send the form
        var submitButton = driver.findElement(By.cssSelector("#login-form-actions > button[type=submit]"));
        submitButton.click();

        // wait until contacts are visible
        assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contacts"))));

        // wait until the logout button is visible
        var logoutButton = assertDoesNotThrow(() -> wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#app-nav > div.v-toolbar__content > button"))));
        // logout
        logoutButton.click();

        // wait until the current url contains '/#/home'
        assertDoesNotThrow(() -> wait.until(webDriver -> webDriver.getCurrentUrl().contains("/#/home")));
    }
}
