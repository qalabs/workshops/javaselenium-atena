package pl.qalabs.workshops.javaselenium2._6;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

@ExtendWith(SeleniumExtension.class)
class SeleniumJupiterTestMethodsInjectionTests {

    @Test
    void testWithChrome(ChromeDriver driver) {
        // inject chrome driver and execute the test
        doTest(driver);
    }

    @Test
    void testWithFirefox(FirefoxDriver driver) {
        // inject firefox driver and execute the test
        doTest(driver);
    }

    @Test
    @EnabledOnOs(OS.MAC)
    void testWithSafari(SafariDriver driver) {
        // inject safari driver and execute the test only on macOS
        doTest(driver);
    }

    @Test
    @EnabledOnOs(OS.WINDOWS)
    void testWithEdge(EdgeDriver driver) {
        // inject edge driver and execute the test only on Windows
        doTest(driver);
    }

    private void doTest(RemoteWebDriver driver) {
        driver.get("https://qalabs.pl");
        Assertions.assertEquals("QA Labs - Warsztaty dla Specjalistów IT", driver.getTitle());
    }
}
