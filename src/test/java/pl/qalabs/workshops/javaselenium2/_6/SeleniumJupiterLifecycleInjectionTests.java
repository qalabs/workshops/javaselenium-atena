package pl.qalabs.workshops.javaselenium2._6;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@ExtendWith(SeleniumExtension.class)
class SeleniumJupiterLifecycleInjectionTests {

    private WebDriver driver;

    @BeforeEach
    void beforeEach(ChromeDriver driver) {
        // inject the driver and assign to the field
        this.driver = driver;
    }

    @Test
    void test1() {
        doTest(driver);
    }

    @Test
    void test2() {
        doTest(driver);
    }

    private void doTest(WebDriver driver) {
        driver.get("https://qalabs.pl");
        Assertions.assertEquals("QA Labs - Warsztaty dla Specjalistów IT", driver.getTitle());
    }
}
